1) Quins cicles formatius s’imparteixen en l’institut?

//cicle

2) Quins cicles formatius de grau mitjà s’imparteixen en l’institut?

//cicle[@grau='Mig']

3) Quins són els noms dels alumnes suspesos?

//alumne[nota<5]

4) Quantes dones cursen el cicle d’ASIX?

//classe[@nom='ASIX']/alumne[genere='Dona']

5) Quins alumnes suspesos tenen un compte a Hotmail?

//alumne[@aprovat='NO']/email[contains(text(),'hotmail')]

6) Quina és la llista de classe d’SMX?

//classe[@nom='SMX']/alumne

7) En quins cicles hi ha alumnes que han tret un 10?


8) Quants alumnes han aprovat ASIX?

9) Quin percentatge d’aprovats té el centre?