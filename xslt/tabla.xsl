<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <style>
                    .rojo{
                        background-color: red;
                    }

                </style>
            </head>
            <body>
                <table border="1">
                    <tr>
                        <th>Titulo</th>
                        <th>Año</th>
                        <th>Precio</th>
                    </tr>

                        <xsl:for-each select="//libro">
                            <xsl:sort select="precio"/>
                            <tr>
                            <xsl:if test="precio &gt; 99">
                                <td class="rojo"><xsl:value-of select="titulo"/></td>
                            </xsl:if>
                            <xsl:if test="precio &lt; 100 ">
                            <td><xsl:value-of select="titulo"/></td>
                            </xsl:if>
                            <td><xsl:value-of select="@año"/></td>
                            <td><xsl:value-of select="precio"/></td>
                            </tr>
                        </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>